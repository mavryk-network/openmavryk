---
id: introduction
title: Introduction
authors: Tezos Ukraine
---

:::info Authors
This content was created by [Tezos Ukraine](https://tezos.org.ua/en) under MIT Licence, and integrated on OpenTezos by Nomadic Labs. The original version can be found [here](https://indexers.tezos.org.ua/) in multiple languages.
:::

# How to Use Blockchain Indexers on Tezos

This is Tezos Ukraine’s introductory course on using blockchain indexers for app development on Tezos. Learn what blockchain indexers are and how to use TzKT, TzStats, Que Pasa, DipDup, or Dappetizer in your apps.

We'll start with the basics: explain how databases work and why indexes speed up searching through them, talk about the different Tezos blockchain indexers, and show you how to use them in small projects.

1. [How Blockchain Indexers Work and Why We Need Them](../how_indexers_work)
    - What are databases and indexes
    - What data can be obtained from an indexer
    - Where indexers are used

2. [Full and Selective Indexers on Tezos](../full_and_selective_indexers)
    - The differences between full and selective indexers
    - An overview of the features of full TzKT and TzStats
    - Short summaries of selective Que Pasa, DipDup and Dappetizer

3. [How to Use the TzKT API in a Simple Project on Tezos](../tzkt)
    - Display the balance of the address 
    - Display liquidity backing stats
    - Improve the project

4. [How to Use Que Pasa in a Simple Project on Tezos](../que_pasa)
    - Install and configure Que Pasa and PostgreSQL
    - Index data from the Signum's smart contract  
    - Display indexed data in the project

5. [How to Use DipDup and Dappetizer on Tezos](../dip_dup_dappetizer)
    - Install and set up DipDup and Dappetizer
    - Do a simple project with DipDup
    - Create a similar project in Dappetizer
