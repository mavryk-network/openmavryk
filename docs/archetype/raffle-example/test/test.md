---
id: unit-test
title: Testing
authors: Benoit Rognier
---

This section presents the testing of the raffle contract.

:::info
The code source of the raffle contract and the test scenario are available in this [repository](https://github.com/completium/archetype-raffle).
:::

## Test scenario

The first time, run the following command to install the [Completium](https://archetype-lang.org/docs/installation) and [mocha](https://www.npmjs.com/package/mocha) libraries:

```bash
npm i
```

Run the test with:

```bash
npm test
```

The test scenario is made of 14 steps. Below is the trace returned by the command above:
```bash

   Initialisation
    ✔ Configure experiment
    ✔ set_mockup
    ✔ set_mockup_now

  [RAFFLE] Contract deployment
    ✔ Deploy raffle

  [RAFFLE] Open Raffle
    ✔ The unauthorized user Alice unsuccessfully calls 'initialise' entrypoint.
    ✔ Owner unsuccessfully calls 'initialise' entrypoint with wrong 'close_buy'.
    ✔ Owner unsuccessfully calls 'initialise' entrypoint with wrong 'reveal_fee'.
    ✔ Owner unsuccessfully calls 'initialise' entrypoint by sending not enough tez to the contract.
    ✔ Owner successfully calls 'initialise' entrypoint.
    ✔ Owner unsuccessfully calls 'initialise' entrypoint because a raffle is already initialised.

  [RAFFLE] Test 'buy' entrypoint (at this point a raffle is open)
    ✔ Alice unsuccessfully calls 'buy' by sending a wrong amount of tez.
    ✔ Alice unsuccessfully calls 'buy' entrypoint because raffle is closed.
    ✔ Alice successfully calls 'buy' entrypoint.
    ✔ Alice unsuccessfully calls 'buy' entrypoint because she has already bought one.
    ✔ Jack successfully calls 'buy' entrypoint.
    ✔ Bob successfully calls 'buy' entrypoint.

  [RAFFLE] Players reveal their raffle key (at this point a raffle is open and two players participated)
    ✔ Alice unsuccessfully calls 'reveal' entrypoint because it is before the 'close_date'.
    ✔ set mockup time after close buy date
    ✔ Alice unsuccessfully calls 'reveal' entrypoint because of an invalid chest key.
    ✔ Alice successfully calls 'reveal' entrypoint and gets the reveal fee.
    ✔ Alice unsuccessfully calls 'reveal' entrypoint because her raffle key is already revealed.
    ✔ Jack successfully calls 'reveal' entrypoint and gets the reveal fee.

  [RAFFLE] Test 'transfer' entrypoint
    ✔ Owner unsucessfully calls 'transfer' entrypoint because Bob is not revealed.
    ✔ Owner sucessfully calls 'reveal' entrypoint to remove Bob's chest, and gets the unlock reward.
    ✔ Owner sucessfully calls 'transfer' entrypoint to send the jackpot to Jack.


  25 passing (1m)
```

## Raffle key chest
In this tutorial example, the _chest time_ parameter imposed by the contract is `10000000`.

:::warning
Note that it's probably *not* a decent _chest time_ value since it takes only 20s to break on a standard computer ...
:::

### Generate chest value
Player Alice's partial key is `123456`, Player Jack's is `234567`, and Player Bob is `345678`.

To get the timelocked value, the value is first packed (turned into bytes) with the following octez client command `hash data`:
```bash
$ octez-client -E https://ghostnet.smartpy.io hash data '123456' of type nat
Warning:

                 This is NOT the Tezos Mainnet.

           Do NOT use your fundraiser keys on this network.

Raw packed data: 0x050080890f
```

We then use the Completium [timelock-utils](https://github.com/completium/timelock-utils) tool to timelock the packed data:
```bash
$ time ./timelock-utils --lock --data 050080890f --time 10000000
{
  "chest":
    "fbd3aaa288feb385b4e5e880b49291d6de99f3de9497db9db180f4d7c58d96a5eccfc0b889ffabe8bbbceaa6f1ef96e1e18f82f194b4a9cb91dffbdd86a4e683bd8adf81b280efe9d388c2d4f7c4b6a7de87dbd78be5aff2cba3ffabc7fbf6ddd0f9bcfd8d99bab5c882ef9bb79cf3fae5abf9f1ccc1a287918490a9cbddd591bdcae0d6c18bd5c39f9ffbc5f0a1edfb8ef5d5e4a4b2b186d49e9f93effddaffaa89a090dda4e0a6aca094fefd8599a7a6c2c6aee0b5e3f5a3c9b994d8af9d8df9b3f3a5abaaaaf2accdb3efcf92d49aa0f2dfe2fbb0a0e3d58adcabd0d7fa89cca8b9c2d2ad95ccc2f3d094f38983dfa385f8c4d0e0ada5d6c2bde1a1c6afc89a9c9cf4b3e2e2f88ab1cfce8ccc8cacd9e5cd8dc48ae79aa4bcc2c8e2dce79df3eb9c840545f99742620f19df2990e22bff98ea02856b7e9a3eb0c9a700000015e0ac8f0a64d17ec13a58c10f11086a0e8b8914b362"
  "key":
    "bef9acb2fec2eafe8fbdacc1cf94addaeea4afc193888ab286eae4a6c18bf1e2f0bb84c4d0e9f187babbbbe88afa9f86fdfec4faf0f5b1dfd98cfdff8dbcf3d4ea9189bddbe3f3b9f0b4ff958ba7ec81a8a893f983c8b5a5b3988486f8c3ac99eda6add8e6afd5fd8cfbbce3faeda1dff3dca7a9e3f5f79888bde3f4c0e682f4cfe9c8a9dcd989e2af9de1f284949a97c2f5b795eb9296e0b1f596a3f7fda69db2faa8f6fe829eb9b1a8e683a9c283eefe98c3f8e49789b4bab6bba988df9af79cd1bcf9efe1d1eaa3f6e5dda9f5afc7d6cfe0949880d1b3fca7fdc6ea9096fce2dcc6da87b888f69395cbb3aaa6caade9adb1bedf82e69cecda959ff8c9cb8bb1b3bc8ce6acc6c89ce7d69bb499b2d58eff84c8ccefc8dcfda1f7d6918bffb48bcba5a90ba488feae859887bbfbdfe083c3b2e48ca2e0bc8c8cbcdbe3fec9a8d7d4c6f8ddf2bcdada95edf1e489aed5e7bd8ba9f2dbf9a2de92a9eafc92db8184c8b1ede699cc92c0bf8fadb8f1c0a1c3a6d1ad8481a6a3c98fcdf9c0f0b1a2fd9985e8f1ce8a87a7b0dbe6faadbcb5b3c19097b5f198a795eff3b78ae3bba394d3f1c6b4d8e3d6b7cdb7ffdcbe94d0bfb485dcfeca9bb3d7d6d0a7f0aeacced6848f86efede9f0a6f99fb4abd7d285dbd6a0b5fb928ce29fc5fad397b6da8d94f187a78785edf088d3cde8cb8187cf99f4eaab90b5f8debcca83828cfccf80afa6d981c2f4d1eacdc6e8dbb2cecba6eaf5968cb69be4ccc78b9ac8809a83a39988bd85ba9dacabe5dfd383bcee8efbd8a5a78d84e3d3d2b4849ddbe4bceb8db9b687a5d5b0a2b0ba0586bf99a2b4a8eca1eae0fc82db87b9f58ca3c1c6d4f0b5b2f684c6aec2e5d2b1bbb297b2ceeefef8cb9581f9bcdfb8969aeda6f7b2d292f09dd893efaf81a7e4cbacd29ed6cee8fbfdd1f0a1bab18adaa1a5d5d9defbcdcfbc9bf184e7c9e4d8efbae2fade9a88f1e7a9ccd8e4e3b5bfd48ea6dcfbabbbfbb0c7c1e0a880f6cbacb9eab5a897e595e09ffcd19ce0f684add4f2e69b9ff892aefeaefe8cadd8ea80c2e18992b193a29dd0ceccc0e3dcb2f7a4d4d38083d9f4ea8b83edd7dbf78ecf9684efb2c2eb96a6b8bac7ac84c0b096e0a0b2f0bae5859fa1ec91d394e8a695a7f1b4f0f9bef3fee7eac1a0a7becdf7b1faec8082a7e7da888dbbf2a1cef2f4b3d1e0fdd8f99993a7c9e6d384ea94c19cddccebf4f288b5e0e3c489aaafd8a0ecc2cf028fa0ff8a92fa8584caaaf9a0e0b8f286d7ad01"
}
./timelock-utils --lock --data 050080890f --time 10000000  0.10s user 0.02s system 97% cpu 0.126 total
```
The timelock encryption generates a chest value, and the key to unlock it.

:::info
In the test scenario, Bob generates the chest value with the wrong time value `10000001`. As a result, the call to `reveal`  removes Bob as a player.
:::

### Crack chest

The following command is used to compute the chest key (ie. crack chest):
```bash
$ time ./timelock-utils --create-chest-key --chest fbd3aaa288feb385b4e5e880b49291d6de99f3de9497db9db180f4d7c58d96a5eccfc0b889ffabe8bbbceaa6f1ef96e1e18f82f194b4a9cb91dffbdd86a4e683bd8adf81b280efe9d388c2d4f7c4b6a7de87dbd78be5aff2cba3ffabc7fbf6ddd0f9bcfd8d99bab5c882ef9bb79cf3fae5abf9f1ccc1a287918490a9cbddd591bdcae0d6c18bd5c39f9ffbc5f0a1edfb8ef5d5e4a4b2b186d49e9f93effddaffaa89a090dda4e0a6aca094fefd8599a7a6c2c6aee0b5e3f5a3c9b994d8af9d8df9b3f3a5abaaaaf2accdb3efcf92d49aa0f2dfe2fbb0a0e3d58adcabd0d7fa89cca8b9c2d2ad95ccc2f3d094f38983dfa385f8c4d0e0ada5d6c2bde1a1c6afc89a9c9cf4b3e2e2f88ab1cfce8ccc8cacd9e5cd8dc48ae79aa4bcc2c8e2dce79df3eb9c840545f99742620f19df2990e22bff98ea02856b7e9a3eb0c9a700000015e0ac8f0a64d17ec13a58c10f11086a0e8b8914b362 --time 10000000
fbd3aaa288feb385b4e5e880b49291d6de99f3de9497db9db180f4d7c58d96a5eccfc0b889ffabe8bbbceaa6f1ef96e1e18f82f194b4a9cb91dffbdd86a4e683bd8adf81b280efe9d388c2d4f7c4b6a7de87dbd78be5aff2cba3ffabc7fbf6ddd0f9bcfd8d99bab5c882ef9bb79cf3fae5abf9f1ccc1a287918490a9cbddd591bdcae0d6c18bd5c39f9ffbc5f0a1edfb8ef5d5e4a4b2b186d49e9f93effddaffaa89a090dda4e0a6aca094fefd8599a7a6c2c6aee0b5e3f5a3c9b994d8af9d8df9b3f3a5abaaaaf2accdb3efcf92d49aa0f2dfe2fbb0a0e3d58adcabd0d7fa89cca8b9c2d2ad95ccc2f3d094f38983dfa385f8c4d0e0ada5d6c2bde1a1c6afc89a9c9cf4b3e2e2f88ab1cfce8ccc8cacd9e5cd8dc48ae79aa4bcc2c8e2dce79df3eb9c8405abd2b3f1f1ea9aceacecdbabe7bcfffeff96eebaa5d184eb98fdfcffabe29a8bece4f497b5ce90e5c4e180dd94fa89ccbba6ecab9c9ad0ea8cbaafd5e5dce6c9dd81e389d989bf88d9fdcefab0e3bbc8c898f29ea983a4bbd8c18ca0cfe5bcd78487aef2b1aac3e8efdba4cabc84b1c18ed398eb9fa0c8edc888bbbc888bb7baa1c9a1fdb3bf8a8ed2bcdbecd1ca9ada8ebafefc8298958ed9a6dcefb5dc8dc9ccde8de79cfa91d6adc9b2dea1ceac91c1f09fe8de99ef9cbfd4a3efc9a3d2ebcce7d5d1b8d0facbf9bfcc81e1edaef09b8ecbd2b2869fab98eb95878bde999d8da9ef9a88c2b8edbdd6a8d5c1f8a3eabad080b2e2bda090a992d7adb0eed4b1c884f0a1e2eab0ccfee0f3d6fee1cca78783bef088aec5f0bbf5d59dfae3abceeeeaa70ad8f3b4dbc1c4f7f9eee7ddfe9bd3d6de838f82e085a491ffebb6fef4bcaa8c91c1d7ebd89bffbeded9b58f989f8b8de5c1a5cfa49a8cd7c592abe5c6c0ada39fb4b1c3d99d95e1cbc1ebb5e083c1f2c5c0e1baaade8999f9aa8caed7eae286ec8c839bdbe78a99a39bdeb8eea3d5d9efa3dc8ae2e19bd6cea7d4e38dc9b98588c289baa5a4dfbfb9d0f39587adc3a5bcf3fd9af5eff4aefddaedd7f587f0829382f5d0ab859283a4c4c4a7aeb2e886b1c197f5fd9d8bd493a9fbdfe3eba8c2b0e4dabff582c080d3d0c099a0e78dc2abd9eca5eee580b6bd86d6b59da0f69fe5c3e0f6efc5feac9fd1d199f7f3ecae97e1bbfba6d08a90bcd7ffbed6b8c1e4ead7f1fbef84e8c7c4eb86bfc0f8a69a9fa8e596d8d7feebbe8ae1e0e191c5c7d39097e1910701
./timelock-utils --create-chest-key --chest  --time 10000000  19.45s user 0.03s system 99% cpu 19.491 total
```

:::info
Note that this generates another key that also opens the chest.
:::

