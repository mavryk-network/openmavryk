---
id: introduction
title: Introduction
slug: /gaming/unity-sdk/
author: Mathias Hiron, Efe Kucuk, and Tim McMackin
---

**Note**: This information is a copy of the Unity SDK documentation at https://docs.tezos.com/unity.

The Tezos SDK for Unity provides tools that let you access user wallets and Tezos in games and other Unity projects.
You can use Tezos via the SDK to:

- Use a player's Tezos account as their account for a game and their wallet as their way of logging in to the game
- Accept payments from players in tez
- Use Tezos to create game assets, store player inventories, and transfer assets between players
- Verify that users own specific game assets and allow them to sell or share them to other players
- Use Tezos smart contracts as backend logic for games

## Installation and use

For a walkthrough of installing and using the SDK in an existing Unity project, see [Quickstart](./quickstart).

## Tutorial scenes

The SDK includes tutorial scenes that demonstrate how to use the SDK.
For information about setting up and using the scenes, see [Tutorial scenes](./scenes).

## SDK objects

The SDK provides objects that you can use to interact with user wallets and with Tezos.
See [Unity SDK reference](./reference).

## Dependencies

The Tezos SDK uses modified versions of the following libraries for communication:

- **Airgap Beacon SDK**: Interacts with Tezos wallets through the Beacon standard for iOS, Android, and WebGL platforms.
- **Netezos**: Interacts with Tezos wallets through the Beacon standard for Windows, Linux, and MacOS platforms. Also prepares parameters for smart contract calls and interprets complex data returned by the ReadView method.

The SDK also uses the [Newtonsoft JSON Unity Package](https://docs.unity3d.com/Packages/com.unity.nuget.newtonsoft-json@3.2/manual/index.html).

## Supported Platforms

The SDK supports Windows, Linux, MacOS, iOS, Android, and WebGL platforms.

## Financial Grants & Support
The Tezos Foundation offers grants and further support for top games utilizing the Tezos SDK for Unity. Take this opportunity to bring your game to life and receive the backing you need to succeed.

>You can apply for a grant [here](https://tezos.foundation/grants/).

## Tezos Community Discord
Looking to get further support and share your work? Join or [Discord](https://discord.com/invite/hsSEtAb9er/) today and reach us out!



