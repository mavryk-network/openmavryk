---
id: decentralized-fundraising
disable_pagination: true
title: Decentralized Fundraising
author: Daniel Nomadic
---

Decentralized fundraising is a new ﬁnancing method allowing project creators to raise funds in a decentralized manner. In the same way that start-ups receive capital before launching, projects using the decentralized fundraising process can receive funds both from institutional and individual investors. Unlike an initial public oﬀering (IPO), investors never own any equity in the project. A major diﬀerence compared to classic fundraising is much lighter legal requirements, which makes the whole process easier.
By participating in decentralized fundraising, investors receive tokens proportionally to their investment. These tokens play a key role in the new applications and protocols built.

### Initial DEX offerings

An initial DEX oﬀering (IDO) is a fundraising process whose purpose is to ﬁnance DeFi projects using decentralized exchanges as an alternative to centralized exchanges that involve heavy and permissioned fundraising processes.
IDOs oﬀer new projects the opportunity to create and list their token to ﬁnance their future business.
To perform an IDO, projects create a public coin on a DEX and receive funds from various investors. IDOs are the decentralized versions of initial exchange oﬀerings (IEOs) where crypto projects launch their token and raise funds via a centralized exchange.

### Decentralized Fundraising on Tezos

[Instaraise](https://instaraise.io/) is the ﬁrst decentralized IDO platform built on the Tezos blockchain.
The platform allows token-based projects to raise funds by setting up a swap pool based on a ﬁxed purchase rate. These so-called “Fixed Swap Pools” have many advantages for token sale investors over traditional fundraising models like initial coin oﬀerings (ICOs), IEOs, and IDOs (Initial DEX Oﬀerings). Fixed Swap Pools will maintain the token price throughout the sale until the initial supply is bought.
With Instaraise, Tezos projects will be able to raise and exchange capital cheaply and quickly, and users will be able to participate in a secure and compliant environment. Instant liquidity provisioning (in QuipuSwap) is facilitated securely thanks to smart contracts.
Anyone can use the presale infrastructure and the owner console to start their presale. Instaraise deployed the $INSTA token to allow presale facilitation and create beneﬁts for users such as liquidity mining (a program that rewards users of a given service), staking rewards, discounted sales with whitelisted addresses, and airdrops.
